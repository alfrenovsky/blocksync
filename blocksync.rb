#!/usr/bin/env ruby
#
BLOCKSIZE=1024*1024*4

require 'open-uri'

module Clone
  def self.blocksize
    BLOCKSIZE
  end

  def self.backup(src,dst)
    srcsize=File.realsize(src)
    puts "Backing up #{srcsize} bytes from #{src}"

    src_blocks=File.find_data(src)

    normalized_blocks=src_blocks.map do |b|
	    ((b[0]/blocksize).to_i..(b[1]/blocksize).to_i).to_a
    end.flatten.sort.uniq.map{|b| [b*blocksize,(b+1)*blocksize-1]}

    index=[]
    bc=0
    rc=0

    normalized_blocks.each do |b|
		  print "Saving #{rc}/#{bc}\r"
      start=b[0]
      finish=b[1]
      b=Block.new(blockdev: src, start: start, finish: finish, directory: dst)
      rc +=1 if b.backup
      index << {start: start, finish: finish, hash: b.hash}
      bc +=1
    end
    puts

    len=srcsize.to_s.size


    out=""
    index.each do |b|
      out << "#{b[:start].to_s.rjust(len)} #{b[:finish].to_s.rjust(len)} #{b[:hash]}\n"
    end

    File.write("#{dst}/index",out)

  end

  def self.restore(src,dst)
	  puts "Restoring from #{src}"
	  index=open("#{src}/index"){|f| f.read}.split("\n").map do |line|
		  start,finish,hash=line.gsub(/\ +/," ").strip.split
		  start=start.to_i
		  finish=finish.to_i
		  {start: start, finish: finish, hash: hash}
	  end

	  bc=0
	  rc=0

	  index.each do |b|
		  print "Restoring #{rc}/#{bc}\r"
		  b=Block.new(blockdev: dst, start: b[:start], finish: b[:finish], directory: src, hash: b[:hash])
		  rc += 1 if b.restore
	          bc += 1

	  end
	  puts
  end
end

class Block
  require 'fileutils'
  require 'digest'

  attr_reader :blockdev, :start, :finish, :directory

  def initialize(params)
    @blockdev=params[:blockdev]
    @start=params[:start]
    @finish=params[:finish]
    @directory=params[:directory]
    @hash=params[:hash]
  end

  def size
    finish-start+1
  end

  def content_from_block
    @content_from_block ||= IO.binread(blockdev,size,start) || ""
  end

  def content_from_file
    @content_from_file ||= open("#{fullpath}"){|f| f.read}
  end

  def hash
    @hash ||= hash_from_block
  end

  def hash_from_block
    @hash_from_block ||= Digest::SHA256.hexdigest(content_from_block)
  end

  def datadir
    "#{directory}/data"
  end

  def path
    "#{datadir}/#{hash[0]}/#{hash[1]}"
  end

  def fullpath
    "#{path}/#{hash}"
  end

  def backup
    FileUtils.mkpath(path) unless File.directory?(path)
    if File.file?(fullpath)
	    false
    else
	    IO.binwrite(fullpath,content_from_block)
	    true
    end
  end

  def restore
	  return false if hash_from_block == hash
	  IO.binwrite(blockdev,content_from_file,start)
	  true
  end
end

class File
  def self.realsize(path)
    io=self.open(path, mode: "rb")
    io.seek(0,IO::SEEK_END)
    io.pos
  end


  def self.find_data(path)

    io=self.open(path, mode: "rb")

    io.seek(0,IO::SEEK_END)
    eof=io.pos
    io.seek(0,IO::SEEK_SET)

    list=[]

    if blockdev?(path)
      list << [0,eof]
    else
      until io.pos == eof
        begin
          io.seek(io.pos,IO::SEEK_DATA)
          start= io.pos
        rescue
          io.seek(io.pos,IO::SEEK_END)
          start=nil
        end

        io.seek(io.pos,IO::SEEK_HOLE) unless (start.nil?)
        finish = io.pos-1
        list <<[start,finish] unless start.nil?
      end
    end
    list
  end
end



